#include <stdio.h>


int isprime(long int n) {
  if (n <= 1) return 0;
  for (long int i = 2; i < n; i++) {
    if (n % i == 0) return 0; 
  }
  return 1;
}


int main() {
  long int prime = 600851475143;

  printf("Prime factors of %ld: ", prime);
  for (long int p = 2; p < prime; p++) {
    if (isprime(p)) {
      while (1) {
        if (prime % p == 0) {
          printf("%d ", p);
          prime /= p;
        } else {
          break;
        }
      }
    }
  }
  printf("%d\n", prime);
}
