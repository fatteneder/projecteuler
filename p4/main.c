#include <stdio.h>
#include <math.h>


// product of three digit numbers has at most 8 digits
/*
 * INPUTS:
 * - MAX_FACTOR ... maximum for each factor in the product for the palindrome
 * - MAX_N_DIGITS ... maximum number of digits for the palindrome, must be adjusted
 *   accordingly with MAX_FACTOR
 */
#define MAX_FACTOR 999
#define MAX_N_DIGITS 8


/*
 * Extract digits of a number n into array `digits`.
 */
int digits[MAX_N_DIGITS];
void getdigits(int n) {
  for (int i = MAX_N_DIGITS; i >= 1; i--) {
    int pow10 = (int) pow(10.0, (double) i-1);
    int r = floor(n / pow10);
    digits[MAX_N_DIGITS-i] = r;
    n -= r * pow10;
  }
}


/*
 * Check if number n is a palindrome, e.g. it reads the same from left to right
 * and right to left.
 * 9009 is a palindrome, 9019 isn't one.
 */
int ispalindrome(int n) {
  getdigits(n); // fill digits

  // debug
  /** for (int i = 0; i < MAX_N_DIGITS; i++) { */
  /**   printf("%d", digits[i]); */
  /** } */

  // compute an offset for the digit index that points to the most significant digit
  // also compute an offset_center which points to the last digit to consider when
  // checking if n reads the same from left to right or reversed.
  // E.g.
  // n    -> (offset, offset_center)
  // 9009 -> (0, 1)
  // 0909 -> (1, 1)
  // 0099 -> (2, 2)
  int offset = 0;
  for (int i = 0; i < MAX_N_DIGITS; i++) {
    if (digits[i] != 0) {
      offset = i;
      break;
    }
  }
  int offset_center = floor((MAX_N_DIGITS-offset)/2) + offset - 1;

  //debug
  /** printf(" %d %d", offset, offset_center); */
  /** printf("\n"); */

  // actual palindrome check
  for (int i = offset; i <= offset_center; i++) {
    if (digits[i] != digits[MAX_N_DIGITS-i+offset-1]) return 0;
  }
  return 1;
}


int main() {

  // test ispalindrome
  /** int p = 9109; */
  /** printf("Is palindrome? %d\n", ispalindrome(p)); */

  // brute force the result
  int n1 = MAX_FACTOR, n2 = MAX_FACTOR;
  int max_p = -1;
  for (int n1 = MAX_FACTOR; n1 > 0; n1--) {
    for (int n2 = MAX_FACTOR; n2 > 0; n2--) {
      int p = n1 * n2;
      if (ispalindrome(p) && p > max_p) {
        max_p = p;
      }
    }
  }

  // just to make sure
  if (ispalindrome(max_p)) {
    printf("Result: %d\n", max_p);
  } else {
    printf("Search failed!\n");
  }
}
