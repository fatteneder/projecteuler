#include <stdio.h>
#include <math.h>


int issquare(int n) {
  int nsqrt = (int) floor(sqrt(n));
  if (n == nsqrt*nsqrt) {
    return 1;
  } else {
    return 0;
  }
}

int main() {

  int sum = 1000;
  int a, b, c = sum+1;
  int searching = 1;
  while (c > 0 && searching) {
    c--;
    b = c-1;
    for (b; b > 0; b--) {
      a = sum - c - b;
      if (a > 0 && a < b && b < c && a*a + b*b - c*c == 0) {
        searching = 0;
        break;
      }
    }
  }

  int abc = a*b*c;

  printf("(a,b,c): (%d,%d,%d)\n", a, b, c);
  printf("abc: %d\n", abc);
}
