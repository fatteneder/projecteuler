#include <stdlib.h>
#include <stdio.h>
#include <math.h>


int n_digits(int n) {
  return (int) log10(n);
}


int n_chars_tens_prefix(int n) {
  if (20 <= n && n < 30)
    return 6; // twenty
  else if (30 <= n && n < 40)
    return 6; // thirty
  else if (40 <= n && n < 50)
    return 5; // forty
  else if (50 <= n && n < 60)
    return 5; // fifty
  else if (60 <= n && n < 70)
    return 5; // sixty
  else if (70 <= n && n < 80)
    return 7; // seventy
  else if (80 <= n && n < 90)
    return 6; // eighty
  else if (90 <= n && n < 100)
    return 6; // ninety

  printf("out of range for %d\n", n);
  exit(-1);
}


int n_chars_special(int n) {
  switch(n) {
    case 1: return 3; // one
    case 2: return 3; // two
    case 3: return 5; // three
    case 4: return 4; // four
    case 5: return 4; // five
    case 6: return 3; // six
    case 7: return 5; // seven
    case 8: return 5; // eight
    case 9: return 4; // nine
    case 10: return 3; // ten
    case 11: return 6; // eleven
    case 12: return 6; // twelve
    case 13: return 8; // thirteen
    case 14: return 8; // fourteen
    case 15: return 7; // fifteen
    case 16: return 7; // sixteen
    case 17: return 9; // seventeen
    case 18: return 9; // eigthteen
    case 19: return 8; // nineteen
  }
}


int n_chars(int n) {

  if (n == 1000)
    return 11; // one thousand

  int nc = 0;
  int n_tens = n % 100;
  if (n_tens > 0 && n_tens < 20)
    nc += n_chars_special(n_tens);
  else if (n_tens >= 20) {
    int ls = n_tens % 10; // least significant
    if (ls > 0)
      nc += n_chars_special(ls);
    nc += n_chars_tens_prefix(n_tens);
  }

  if (n > 100) {
    nc += 7; // hundred
    int ms = (int) floor(n / 100); // most significant
    nc += n_chars_special(ms);
    if (n % 100 != 0) // add an 'and'
      nc += 3;
  }
  return nc;
}


int main() {

  printf("#three hundred and forty-two = %d (23)\n", n_chars(342));
  printf("#one hundred and fifteen = %d (20)\n", n_chars(115));
  printf("#fifteen = %d (7)\n", n_chars(15));
  printf("#two hundred = %d (10)\n", n_chars(200));
  printf("#two hundred and one = %d (16)\n", n_chars(201));
  printf("#one thousand = %d (11)\n", n_chars(1000));
  printf("#two hundred and twenty-two = %d (22)\n", n_chars(222));
  printf("#forty-one = %d (8)\n", n_chars(41));
  printf("#seven hundred and sixty-three = %d (25)\n", n_chars(763));
  printf("#three hundred and seventy-three = %d (27)\n", n_chars(373));
  printf("#twenty-one = %d (9)\n", n_chars(21));

  int total = 0;
  for (int i = 1; i <= 1000; i++)
    total += n_chars(i);
  printf("#chars for all numbers in 1:1000: %d\n", total);

  return 0;
}
