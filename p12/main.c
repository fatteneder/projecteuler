#include <stdio.h>


int main() {
  int triagnr = 0;
  int maxdivs = 0;
  int increment = 1;
  while (maxdivs <= 500) {
    triagnr += increment;
    increment++;
    int divs = 0;
    // d * pd = triagnr
    int pd = triagnr;
    for (int d = 1; d < pd; d++) {
      if (triagnr % d == 0) {
        pd = triagnr / d;
        divs += 2; // + 2 because d and pd divide triagnr
      }
    }
    if (divs > maxdivs) {
      maxdivs = divs;
      printf("Nr of divisors: %d\n", maxdivs);
    }
  }

  printf("Nr of divisors for triangle nr %d: %d\n", triagnr, maxdivs);
}
