#include <stdio.h>


int main() {
  int n_numbers = 100;
  int sum_sqr = 0;
  int sum = 0;
  for (int n = 1; n <= n_numbers; n++) {
    sum_sqr += n*n;
    sum += n;
  }
  int sqr_sum = sum*sum;

  int diff = sqr_sum - sum_sqr;

  printf("Square of Sum - Sum of Squares: %d\n", diff);
}
