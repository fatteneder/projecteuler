#include <stdio.h>

int main() {
  int max_divider = 20;

  int result = max_divider;
  while (result < 1e10) {
    int dividesall = 1;
    for (int div = 1; div <= max_divider; div++) {
      if (result % div != 0) {
        dividesall = 0;
        break;
      }
    }
    if (dividesall) break;
    result++;
  }

  printf("Smallest number that is divided by 1-%d: %d\n", max_divider, result);
}
