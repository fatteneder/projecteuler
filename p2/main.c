#include <stdio.h>


int main() {
  int sum = 0;
  int n1 = 1, n2 = 2;
  int max_n = 4e6;
  while (n1 < max_n && n2 < max_n) {
    if (n2 % 2 == 0) sum += n2;
    int tmp = n1 + n2;
    n1 = n2;
    n2 = tmp;
  }
  
  printf("Result: %d\n", sum);
}
