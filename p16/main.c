#include <stdlib.h>
#include <stdio.h>
#include <math.h>


int main() {

  // exponent of 2: 2^n
  int n = 1000;

  {
    printf("Binary representation (reversed): ");
    int tmp = n;
    while (tmp) {
      putc((tmp & 1) ? '1' : '0', stdout);
      tmp >>= 1;
    }
    printf("\n");
  }

  int ndigits = (int) ceil(n * log(2.0) / log(10.0));
  int *digits = calloc(ndigits, sizeof(int));

  if (digits == NULL)
    return -1;

  digits[0] = 1;
  for (int ni = 0; ni < n; ni++) {
    /** for (int i = 0; i < ndigits; i++) */
    /**   printf("%d", digits[ndigits-i-1]); */
    /** printf("\n"); */
    int carry = 0;
    for (int i = 0; i < ndigits; i++) {
      int di = digits[i];
      digits[i] = (di*2 + carry) % 10;
      carry = (int) floor((2*di+carry)/10.0);
    }
  }

  int digitsum = 0;
  for (int i = 0; i < ndigits; i++) {
    int di = digits[ndigits-i-1];
    printf("%d", di);
    digitsum += di;
  }
  printf("\n");
  printf("sum of digits: %d\n", digitsum);

  free(digits); digits = NULL;

  return 0;
}
