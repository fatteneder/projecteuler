#include <stdio.h>
#include <stdlib.h>


long long collatz_step(long long n) {
  if (n % 2 == 0) {
    return (long long) n / 2;
  } else {
    return (long long) 3 * n + 1;
  }
}

int main() {
  int max_count = 1;
  int start_nr = 1;
  for (int init_state = 1; init_state < 1e6; init_state++) {
    int count = 1;
    long long state = (long long) init_state;
    while(state != 1) {
      state = collatz_step(state);
      count++;
    }
    if (count > max_count) {
      max_count = count;
      start_nr = init_state;
    }
  }

  printf("Max number of steps for start number: (%d,%d)\n", max_count, start_nr);
}
