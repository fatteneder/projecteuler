#include <stdio.h>


#define MAX_PRIMES 10001


int main() {
  int primes [MAX_PRIMES];

  int idx = 0;
  int p = 2;
  while (idx < MAX_PRIMES) {
    int isdivisble = 0;
    for (int d = 2; d < p; d++) {
      if (p % d == 0) {
        isdivisble = 1;
        break;
      }
    }
    if (!isdivisble) {
      primes[idx] = p;
      idx++;
    }
    p++;
  }

  printf("%dth prime: %d\n", MAX_PRIMES, primes[MAX_PRIMES-1]);
}
