#include <stdio.h>
#include <stdlib.h>


int main() {
  int n_primes = 10;
  int * primes = malloc(n_primes * sizeof(int));
  int n_filled = 0;
  if (!primes) return -1;
  long long sum = 0;
  int max_n = 2e6;

  primes[0] = 2, n_filled += 1, sum += 2;
  for (int p = 2; p < max_n; p++) {
    int isprime = 1;
    for (int i = 0; i < n_filled; i++) {
      int tp = primes[i];
      if (p % tp == 0) {
        isprime = 0;
        break;
      }
    }
    if (isprime) {
      if (n_filled >= n_primes) {
        printf("Resizing...\n");
        // resize prime storage
        n_primes *= 2;
        primes = realloc(primes, n_primes*sizeof(int));
        if (!primes) return -1;
      }
      primes[n_filled] = p;
      n_filled += 1;
      sum += (long long) p;
    }
  }

  printf("sum : %ld\n", sum);

  free(primes); primes = NULL;
}
