#include <stdio.h>

/*
 * can only move from left to right or up to down.
 *
 * 0---1
 * }   |
 * 2---3
 *
 * - 1 move for 0->1
 * - 1 move for 0->2
 * - 2 moves for 0->3
 *
 * short notation
 * - 1:0->1
 * - 1:0->2
 * - 2:0->3
 *
 * 0---1---4
 * }   |   |
 * 2---3---5
 *
 * - 1:1->4
 * - 1 3->5
 * - 2:1->5
 * - (1:0->1 x 2:1->5 + 1:0->2 x 1:2->3 x 1:3->5) = 3:0->5
 *
 *
 * 0---1
 * }   |
 * 2---3
 * |   |
 * 4---5
 *
 * - 2:2->5
 * - 1:3->5
 * - (1:0->2 x 2:2->5 + 1:0->1->3 x 1:3->5) = 3:0->5
 *
 * 0---1---6
 * }   |   |
 * 2---3---7
 * |   |   |
 * 4---5---8
 *
 * - 2:3->8
 * - 1:7->8
 * - 1:5->8
 * - (3:0:7 x 1:7->8 + 3:0->5 x 1:5->8) = 6:0->8
 *
 * 0---1---6---9
 * }   |   |   |
 * 2---3---7---10
 * |   |   |   |
 * 4---5---8---11
 *
 * - 1:8->11
 * - 2:7->11
 * - 3:6->11
 * - (1:8->11 x 6:0->8 + 3:0->7 x 1:7->10->11 + 1:0->6 x 1:6->9->10->11) =
 *   10:0->11
 *
 * 0---1---6---9
 * }   |   |   |
 * 2---3---7---10
 * |   |   |   |
 * 4---5---8---11
 * }   }   |   |
 * 12--13--14--15
 *
 * - (1:11->15 x 10:0->11 + 1:14->15 x 10:0->14) = 20:0->15
 *
 * deducted rules (n ... number of vertices =^= number of edges + 1)
 * - nsteps for nxn grid =: N(n,n) 
 * - N(n,m)   = N(m,n)
 * - N(1,n)   = 1
 * - N(2,n)   = n
 * - N(n,n)   = N(n,n-1) + N(n-1,n) = 2xN(n-1,n), n > 2
 * - N(n-1,n) = 1xN(n-1,n-1) + 1xN(n-2,n-1) + ... + 1*N(1,n-1), n > 2
 *
 * Test:
 * N(1,1)   = 1
 * N(1,2)   = 1
 * N(2,2)   = 2
 * N(2,3)   = N(2,2) + N(1,2) = 3
 * N(3,3)   = 2*N(2,3) = 6
 * N(3,4)   = N(3,3) + N(2,3) + N(1,3) = 6 + 3 + 1 = 10
 * N(4,4)   = 2*N(3,4) = 20
 * N(2,4)   = 4
 * N(4,5)   = N(4,4) + N(3,4) + N(2,4) + N(1,4) = 20 + 10 + 4 + 1 = 25
 * N(5,5)   = 2*N(4,5) = 50
 */

long long N_n_m(long long n, long long m) {
  if (n == m) {
    return 2 * N_n_m(n-1,m);
  } else if (n == 1) {
    return 1;
  } else if (n == 2) {
    return m;
  } else {
    // n < m && n > 2
    long long N = 0;
    for (long long i = 1; i <= n; i++) {
      N += N_n_m(i,m-1);
    }
    return N;
  }
}

long long main() {
  // 20 does take quite somoe time, use -O3 optimzation
  // TODO Can this also be done with tail recursion?
  // Nevermind, checking the solutions it turns out that this problem 
  // could be solved much simpler using combinatorics.
  // Given a 20x20 grid and we can only move right or doen, then every
  // lattice path could be represented as a string of R (right) and 
  // D (down) moves, the string will always be of length 20+20 because we
  // walk from vertex (1,1) to (21,21). If we arrange the 20 R moves, then
  // we automatically know the 20 D moves (for one path). So the
  // question boils down to how many ways are there to arrange 20 R moves
  // into 40 places? There are at total of (40-20)!/20! ways to arrange
  // the Rs. Since the order does not matter, we divide once more by 20!.
  // Hence, (40-20)!/(20!20!) = BinomialCoefficient(40, 20).
  // Now I am too lazy to implement this too, but I guess one should
  // be careful with the big numbers arising from the factorials.
  long long ne = 20; // number of edges of grid
  long long nv = ne + 1; // number of vertices of grid
  long long N = N_n_m(nv,nv);
  printf("N(%ld,%ld) = %ld\n", nv, nv, N);
}
